import { PhotosProvider } from './../../providers/photos/photos';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { ActionSheetController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';

import { PhotoModel } from '../../models/photoModel';

import 'rxjs/add/operator/map';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  photoUploadList : PhotoModel[] = [];
  photoListSvc : PhotoModel[];
  photoList : PhotoModel[];
  photoListBack : PhotoModel[];
  bCarregando : boolean = true;

  searchTerm : string;
  shouldShowCancel : boolean = false;

  pgStart : number = 0;
  pgLimit : number = 5;
  pgTotal : number = 0;

  constructor(
    public navCtrl: NavController,
    public camera : Camera,
    public actionSheetCtrl: ActionSheetController,
    public photoProvider : PhotosProvider
  ) {

    photoProvider.getPhotos().subscribe((data)=>{
      this.pgTotal = data.length;
      this.photoListSvc = data;
      this.photoList = this.photoListSvc.slice(this.pgStart, this.pgLimit);
      this.photoListBack = this.photoList;
      this.bCarregando = false;
    });
  }

  listPicOptions() {
    this.actionSheetCtrl.create({
      title: 'Opções',
      buttons: [{
        text: 'Camera',
        handler: () => {
          this.takePicture();
        }
      },{
        text: 'Biblioteca',
        handler: () => {
          this.openLibrary();
        }
      },{
        text: 'Cancelar',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    }).present();
  }

  takePicture() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
      this.sendPicture(imageData);
    }, (err) => {
      console.log(err);
    });
  }

  openLibrary() {
    this.camera.getPicture({
      sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
      destinationType: this.camera.DestinationType.DATA_URL
     }).then((imageData) => {
       this.sendPicture(imageData);
      }, (err) => {
       console.log(err);
     });
  }

  sendPicture(imageData : string) {
    var _model = new PhotoModel();
    _model.pic = 'data:image/jpeg;base64,' + imageData;
    _model.title = 'Imagem incluída pelo cel!'

    this.photoUploadList.push(_model);
    this.photoUploadList.reverse();
  }

  onInput() {
    this.shouldShowCancel = true;

    if (this.searchTerm == '') {
      this.photoList  = this.photoListBack;
      return;
    }

    this.photoList = this.photoListBack.filter((photo) => {
      return photo.title.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1;
    });
  }

  onCancel() {
    this.shouldShowCancel = false;
    this.photoList  = this.photoListBack;
    this.searchTerm = "";
  }

  doInfinite(infiniteScroll) {
    this.onCancel();

    this.pgStart++;
    this.photoList = this.photoListBack;

    return new Promise((resolve) => {
      setTimeout(() => {
        this.photoList = this.photoListSvc.slice(0, (this.pgStart + 1) * this.pgLimit);
        this.photoListBack = this.photoList;

        infiniteScroll.complete();
        resolve();
      }, 500);
    });

    
  }

}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import 'rxjs/add/operator/map';
import { PhotoModel } from '../../models/photoModel';

/*
  Generated class for the PhotosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PhotosProvider {
  apiUrl : string = 'https://jsonplaceholder.typicode.com';

  constructor(public http: HttpClient) {
  }

  getPhotos() {
    return  this.http.get<PhotoModel[]>(this.apiUrl + '/photos');
  }

}

export class PhotoModel {
    albumId:number;
    id:number;
    title:string;
    url:string;
    thumbnailUrl:string;
    pic:string;
    data:Date = new Date();
}